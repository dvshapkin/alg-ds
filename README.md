# Algorithms & data structures

[![Crates.io](https://img.shields.io/crates/l/alg-ds)](https://gitlub.com/dvshapkin/alg-ds/LICENSE)


### alg
*Search algorithms:*
<table>
<tr><th>name</th><th>complexity</th><th>description</th></tr>
<tr><td>max</td> <td>O(n)</td> <td>maximum in &[T]</td></tr>
<tr><td>min</td> <td>O(n)</td> <td>minimum in &[T]</td></tr>
<tr><td>binary</td> <td>O(log<sub><small>2</small></sub>n)</td> <td>binary search in &[T]</td></tr>
<tr><td>lcs</td> <td>O(mn)</td> <td>longest common subsequence</td></tr>
</table>

*Sort algorithms:*
<table>
<tr><th>name</th><th>complexity</th></tr>
<tr><td>selection</td> <td>O(n<sup><small>2</small></sup>)</td></tr>
<tr><td>quick</td> <td>O(n log<sub><small>2</small></sub>n)</td></tr>
</table>

### ds
<table>
<tr><th>name</th><th>description</th></tr>
<tr><td>Matrix</td> <td>two-dimensional array</td></tr>
</table>
