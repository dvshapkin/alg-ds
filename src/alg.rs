//! Common algorithms: search, sort and so on.

pub mod lcs;
pub mod search;
pub mod sort;
